<?php

class Robot
{
    const DIRECTIONS = ['north', 'east', 'south', 'west'];

    private $positionX;
    private $positionY;

    public function __construct(string $inputs)
    {
        $this->inputs = $inputs;
        $this->positionX = 0;
        $this->positionY = 0;
    }

    public function walk(): string
    {
        $inputs = strtoupper($this->inputs);

        preg_match_all('/[RLW][0-9]*/', $inputs, $matches);

        $direction = 0;

        foreach ($matches[0] as $match) {

            $key = array_search($direction, self::DIRECTIONS);

            switch ($match) {
                case 'R':
                    $key++;

                    $direction = $this->getDirection($key, 'north');
                    break;

                case 'L':
                    $key--;

                    $direction = $this->getDirection($key, 'west');

                    break;

                default:
                    $value = str_replace('W', '', $match);

                    $position = $this->getPosition($direction, $value, $this->positionX, $this->positionY);
                    $this->positionX = $position['x'];
                    $this->positionY = $position['y'];
                    break;
            }
        }

        return "X: $this->positionX, Y: $this->positionY, Direction: $direction";
    }

    private function getDirection($key, $direction): string
    {

        if ($key == 4 || $key == -1) {
            $key = array_search($direction, self::DIRECTIONS);
        }

        return self::DIRECTIONS[$key];
    }

    private function getPosition($key, $value, $x = 0, $y = 0): array
    {
        switch ($key) {
            case 'north': // x = 0, y = 1
                $y += $value;
                break;

            case 'east': // x = 1, y = 0
                $x += $value;
                break;

            case 'south': // x = 0, y = -1
                $y -= $value;
                break;

            case 'west': // x = -1, y = 0
                $x -= $value;
                break;

            default:
                throw new Exception('something wrong');
                break;
        }

        return [
            'x' => $x,
            'y' => $y
        ];
    }
}
