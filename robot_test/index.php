<?php
spl_autoload_register(function ($class_name) {
    include $class_name . '.php';
});

$inputs = $argv[1];

$robot = new Robot($inputs);
$walk = $robot->walk();

echo "$walk\n";
